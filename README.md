# Badflix
That is a simple video player mada with Javascript, pure HTML and CSS.

## Usage
### Installing
```` 
	git clone [current url]
	cd folder-destination
````
### Downloading Videos
To play videos you need to download them first.
Use the links bellow to make this.

1. [Big Buck Bunny](http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4) (Rename him to big-buck.mp4)
2. [Elephant Dream](http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4) (Rename him to elephants-dream-medium.mp4)
3. [Sintel](http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/Sintel.mp4) (Rename him to Sintel.mp4)

After downloading, put them in the folder named ***videos*** in the folder destination (in the project folder).

***Please note that the previous steps must have been successfully completed for the application to work !***