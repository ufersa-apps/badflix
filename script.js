document.addEventListener("DOMContentLoaded", function() {
  var video = document.querySelector("#video");
  var canvas = document.querySelector("#canvas");
  var context = canvas.getContext("2d");
  var linkVideo = document.querySelectorAll("#link");
  var btnGray = document.querySelector("#grayEffect");
  var btnSummary = document.querySelector("#summaryEffect");
  var btnBlur = document.querySelector("#blurEffect");
  var btnSepia = document.querySelector("#sepiaEffect");

  video.addEventListener("loadedmetadate", function() {
    canvas.style.width = videoWidth + "px";
    canvas.height = videoHeight + "px";
  });

  var cont = 0;
  var draw = function() {
    if (video.paused || video.ended) return;
    var x = 0;
    var y = 0;
    context.drawImage(video, x, y, canvas.width, canvas.height);
    var imageData = context.getImageData(x, y, canvas.width, canvas.height);
    var data = imageData.data;
    for (let i = 0; i < data.length; i += 4) {
      let red = data[i + 0];
      let green = data[i + 1];
      let blue = data[i + 2];
      let media = (red + green + blue) / 3;

      data[i + 0] = media;
      data[i + 1] = media;
      data[i + 2] = media;
    }
    context.putImageData(imageData, x, y);
    var image = new Image();
    image.src = canvas.toDataURL("image/png");
    image.width = 80;
    if (cont++ % 300 === 0) {
      var imgs = document.querySelector("#imgs");
      imgs.appendChild(image);
    }
    requestAnimationFrame(draw);
  };

  video.addEventListener("play", function() {
    if (video.paused || video.ended) return;
    draw();
  });

  for (let i = 0; i < linkVideo.length; i++) {
    linkVideo[i].addEventListener("click", function() {
      var video = document.querySelector("#video");
      canvas.innerHTML = "";
      video.src = linkVideo[i].getAttribute("url");
    });
  }

  btnGray.addEventListener("click", function() {
    var divCanvas = document.querySelector("#divCanvas");
    if (divCanvas.style.display !== "block") {
      btnGray.innerHTML = "Desativar Efeito Gray";
      divCanvas.style.display = "block";
    } else {
      btnGray.innerHTML = "Ativar Efeito Gray";
      divCanvas.style.display = "none";
    }
  });

  btnSummary.addEventListener("click", function() {
    var divSummary = document.querySelector("#divSummary");
    if (divSummary.style.display !== "block") {
      btnSummary.innerHTML = "Desativar Sumarização";
      divSummary.style.display = "block";
    } else {
      btnSummary.innerHTML = "Ativar Sumarização";
      divSummary.style.display = "none";
    }
  });
});

var openFile = function(event) {
  var input = event.target;

  var reader = new FileReader();
  reader.onload = function() {
    var dataURL = reader.result;
    var track = document.querySelector("#track");
    track.src = dataURL;
  };
  reader.readAsDataURL(input.files[0]);
};
