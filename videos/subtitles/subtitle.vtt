WEBVTT

00:00:00.000 --> 00:05:00.000
In my early videos I used the slogan “don’t be a script kiddie” in the intro. And quite
00:05 --> 00:09
some time ago I got the following YouTube comment about it:
00:09 --> 00:14
“Don’t be a script kiddie, is a reference to Mr. Robot? Or is it already a thing in
00:14 --> 00:15
general?
00:15 --> 00:19
Well… mhmh... I think it would be interesting to look for the origin of the term “script
00:19 --> 00:24
kiddie” and at the same time get a glimpse into some hacker culture history. I think
00:24 --> 00:28
this is interesting because it gives us an excuse to look into the past, to better understand
00:28 --> 00:34
on what ou r community is built upon and somewhat honor and remember it. I wish I was old enough
00:34 --> 00:38
to have experienced that time myself to tell you first-hand stories, but unfortunately
00:38 --> 00:44
I’m born in the early 90s and so I’m merely an observer and explorer of the publicly available
00:44 --> 00:48
historical records. But there is fascinating stuff out there that I want to share with
00:48 --> 00:55
you.
00:55 --> 01:01 
The first resource I wanted to check is phrack. I have referenced phrack many times before,
01:01 --> 01:06
because it is I think the longest running ezine with quality content. And the cool thing
01:06 --> 01:12
is that these issues started in 1985 by Taran King and Knight Lightning. So it has been
01:12 --> 
part of hacking history from pretty early on, and I highly encourage you to just randomly
01:18 --> 
click around those old issues and read some articles. You will find stuff about operating
01:22 --> 
systems and various technologies you might have never heard about, because they basically
01:27 --> 
don’t exist anymore. But you also find traces of the humans behind all this through the
01:32 -->
phrack-prophiles and other articles. Maybe checkout the famous hacker manifesto from
01:38 -->
1986 - The Conscience of a Hacker. It was written by a teenager, calling himself The
01:43 -->
Mentor, who probably never thought that his rage induced philosophical writing would go
01:49 -->
on to influence a whole generation of hackers. But it becomes even more fascinating with
01:54 -->
the privilege of being here in the future, right now, and looking back. I found this
02:00 -->
talk from 2002 by The Mentor, he is now a grown man reflecting on his experience about
02:06 -->
this. It’s emotional and human. And in the end this is what the hacker culture is. It’s
02:11 -->
full of humans with complex emotions, we shouldn't forget that.
02:14 -->
Anyway, I’m getting really distracted here, but there are not enough opportunities to
02:18 -->
expose people to this stuff. Back to script kiddie research.
02:22 -->
We can si mply wget the archive from phrack.org. -r, recursive crawling. -l only two layers
02:28 -->
deep and -np for no parent folders. This takes a moment to download.
02:33 -->
And then we can grep. Let’s start with simply script kiddie.
02:36 -->
The oldest occurrence of “script kiddie” we can find is from issue 54, released in
02:42 -->
1998, article 9 and 11.
02:45 -->
when someone posts (say) a root hole in Sun's comsat daemon, our little cracker could grep
02:51 -->
his list for 'UDP/512' and 'Solaris 2.6' and he immediately has pages and pages of rootable
02:57 -->
boxes. It should be noted that this is SCRIPT KIDDIE behavior.
03:02 -->
Mh! And the other is a sarcastic comment about Rootshell.com being hacked and then them handing
03:07 -->
over data to law enforcement:
03:10 -->
Lets give out scripts that help every clueless script kiddie break into thousands of sites
03:13 -->
worldwide. then narc off the one that breaks into us.
03:17 -->
But this issue is from 1998. Still the 90s, but I’m sure there have to be earlier occurrences.
03:23 -->
And we should maybe start looking for slight variations that I think are still valid.
03:27 -->
For example “script kiddy” with a y, but there is not much.
03:31 -->
So I was searching for just “kiddie”, which might help us find other variations
03:35 -->
including skript with a k or skiddie or script-kiddie with a dash.
03:39 -->
But the only earlier occurrences than what we have is of just “kiddies” - for example
03:44 -->
in issue 12 from 87.
03:47 -->
I have been phreaking since these kiddies were still messing their diaper
03:51 -->
But that’s not quite the “script kiddie” terminology. I think we have to expand and
03:55 -->
look somewhere else. Wikipedia is often pretty good with information
04:00 -->
and references, but unfortunately there are only links going back to the 2000s.
04:04 -->
While looking for more textfiles to search through, I stumbled over this repository by
04:09 -->
fdiskyou with a larger collection of Zines. However there was not really anything much
04:14 -->
older than our 1998 phrack article.
04:18 -->
Then I also looked at the textfiles.com archive, which is ran by Jason Scott. And he has a
04:23 -->
huuuge archive of old zines and bulletin boards, mailing lists, and more. And so I started
04:29 -->
to search through that and indeed I found some interesting traces from around 1993/1994
04:34 -->
in a BBS called yabbs - yet another bulletin board system created by alex wetmore in 1991
04:41 -->
at carnegie mellon. The first interesting find is from october
04:45 -->
1993 “Enjoy your K-Rad elite kodez kiddies”. the term kiddie is not prefixed with “script-kiddie”,
04:53 -->
and I’m not sure if it’s “elite code (comma) kiddies”. Or elite “code kiddies”.
05:00 -->
Code and script is almost synonymous and it seems to be used in a very similar derogatory
05:04 -->
way as the modern terminology. In december 1993 the term kiddie is also used
05:09 -->
in relation to “rip them off”, which is also a typical meaning of “script kiddies”
05:14 -->
nowadays - they just steal and take other people’s work.
05:18 -->
And then in June 1994 there is this. “Codez kiddies just don’t seem to understand that
05:22 -->
those scripts had to come from somwhere. Hacking has fizzled down to kids running scripts to
05:27 -->
show off at a 2600 meet.” there is again a reference to “codez kiddies”
05:32 -->
but now the term “script” also starts to appear in the same sentence.
05:36 -->
And then in July 1994 it got combined to “Even 99% of the wanker script codez kiddies knows
05:43 -->
enough to not run scripts on the Department of Defense”.
05:45 -->
Isn’t that fascinating. I start to believe that 1994 is the year where the term “script
05:51 -->
kiddie” started to appear. But this example is still not 100% the modern term. And so
05:56 -->
the earliest usage of literally “script kiddie” I was only able to find in an exploit
06:02 -->
from 1996.
06:04 -->
Crontab has a bug. You run crontab -e, then you goto a shell, relink the
06:08 -->
temp file that crontab is having you edit, and presto, it is now your
06:11 -->
property. This bug has been confirmed on various versions of OSF/1, Digital
06:16 -->
UNIX 3.x, and AIX 3.x
06:18 -->
If, while running my script, you somehow manage to mangle up your whole
06:22 -->
system, or perhaps do something stupid that will place you in jail, then
06:25 -->
neither I, nor sirsyko, nor the other fine folks of r00t are responsible.
06:30 -->
Personally, I hope my script eats your cat and causes swarms of locuses to
06:34 -->
decend down upon you, but I am not responsible if they do.
06:38 -->
(signed) --kmem.
06:40 -->
[-- Script kiddies cut here -- ]
06:41 -->
THERE IT IS!
06:43 -->
This bug was discovered by sirsyko on Thursday 21st Mar of 1996, just after midnight. I guess
06:51 -->
nothing has changed with hacking into the night.
06:53 -->
And this exploit script was written by kmem.
06:56 -->
You know what’s cool? With a bit of digging I actually found party pictures from around
07:01 -->
96/97 from kmem and sirsyko. I find it fascinating to look at these. And I’m so grateful that
07:08 -->
there was some record keeping with pictures from that time, that takes away some of the
07:12 -->
mysticism that surrounds these early hackers. They look like normal dudes!
07:17 -->
But anyway, is this really the first time that somebody used the term script kiddie?
07:21 -->
Is this where it all started? Well… When I was asking around, somebody
07:26 -->
reminded me of Cunningham's Law: "the best way to get the right answer on the
07:31 -->
internet is not to ask a question; it's to post the wrong answer."
07:35 -->
SO I
07:37 -->
DECLARE THIS EXPLOIT
07:39 -->
TO BE THE FIRST! USAGE OF THE TERM SCRIPT KIDDIE!
07:42 -->
IT’S. A. FACT! Mh! Let’s see if that works.
07:46 -->
I’m aware that a lot of the hacking culture happened in private boards, forums and chat
07:50 -->
rooms. But maybe somebody out there has old non-public IRC logs and can grep over it for
07:55 -->
us. I think it would be really cool to trace this further.
07:59 -->
Also I would LOVE to hear the story behind any exploit from the 90s. How did you find
08:04 -->
it, did you share it, how did you learn it, what kind of research did you do yourself,
08:07 -->
who was influential to you, did anybody steal it, were there bug collisions, what was it
08:13 -->
like to experience a buffer overflow for the first time and so forth. I think there are
08:18 -->
many fascinating stories hidden behind those zines and exploits from that time that haven’t
08:23 -->
been told and I don’t want them to be forgotten. Please share your story
 